import "./header.css";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { Link } from "react-router-dom";

function Header() {
    return (
        <div className="header">
            <div className="headerWrapper">
                <div className="headerLeft">
                    <Link to="/spacex-api-react-client" className="headerTitle link">SpaceX Client</Link>
                </div>
                <div className="headerRight">
                    <AccountCircleIcon style={{ fontSize: 40, color: "white", cursor: "pointer" }}/>
                </div>
            </div>
        </div>
    );
}

export default Header;