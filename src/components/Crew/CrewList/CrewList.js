import "./crewList.css"
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {useEffect, useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import SearchBar from "material-ui-search-bar";

function CrewList() {

    const [loading, setLoading] = useState(false);
    const [crew, setCrew] = useState([]);

    const [crewRows, setRows] = useState(crew);
    const [searched, setSearched] = useState("");

    useEffect( () => {
        const loadCrew = async () => {
            setLoading(true);
            const response = await axios.get("https://api.spacexdata.com/v4/crew");
            setCrew(response.data);
            setLoading(false);
        }
        loadCrew();
    }, [])

    const requestSearch = (searchedVal) => {
        const filteredRows = crew.filter((row) => {
            return row.name.toLowerCase().includes(searchedVal.toLowerCase());
        });
        setRows(filteredRows);
    };

    const cancelSearch = () => {
        setSearched("");
        requestSearch(searched);
    };

    return (
        <div className="crewList">
            <h1 className="crewListTitle">List of Crew Members</h1>
            <SearchBar placeholder="Search (ex : Thomas Pesquet)"
                       value={searched}
                       onChange={(searchVal) => requestSearch(searchVal)}
                       onCancelSearch={() => cancelSearch()}
            />
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead style={{backgroundColor: "#598bff"}}>
                        <TableRow>
                            <TableCell style={{color: "white"}} align="center">Name</TableCell>
                            <TableCell style={{color: "white"}} align="center">Agency</TableCell>
                            <TableCell style={{color: "white"}} align="center">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            loading ? <tr>Loading...</tr> : crewRows.map((crewMember) => (
                                <TableRow key={crewMember.name}>
                                    <TableCell component="th" style={{fontWeight: "bold"}} scope="row" align="center">
                                        <Link to={"/crew/" + crewMember.id}>{crewMember.name}</Link>
                                    </TableCell>
                                    <TableCell align="center">{crewMember.agency}</TableCell>
                                    <TableCell align="center">{crewMember.status}</TableCell>
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default CrewList;