import "./crewProfile.css"
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";

function CrewProfile() {

    let { crewId } = useParams();

    const [loading, setLoading] = useState(false);
    const [crewMember, setCrewMember] = useState([]);

    useEffect( () => {
        const loadCrewMember = async () => {
            setLoading(true);
            const response = await axios.get("https://api.spacexdata.com/v4/crew/" + crewId);
            setCrewMember(response.data);
            setLoading(false);
        }
        loadCrewMember();
    }, [crewId])

    return (loading) ? <h4>Loading...</h4> :
        <div className="crewMemberCardContainer">
            <div className="crewMemberCard">
                <div className="crewMemberImageContainer">
                    <img className="crewMemberImage" src={crewMember.image} alt={crewMember.name + " image"} />
                </div>
                <div className="crewMemberDataContainer">
                    <h2 className="crewMemberData">{crewMember.name}</h2>
                    <p className="crewMemberData"><span className="dataBold">Agency : </span>{crewMember.agency}</p>
                    <p className="crewMemberData"><span className="dataBold">Status : </span>{crewMember.status}</p>
                    <p className="crewMemberData"><span className="dataBold">More info here : </span><a href={crewMember.wikipedia}>wikipedia</a></p>
                </div>
            </div>
        </div>
}

export default CrewProfile;