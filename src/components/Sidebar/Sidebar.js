import "./sidebar.css";
import TimelineIcon from '@material-ui/icons/Timeline';
import ListIcon from '@material-ui/icons/List';
import RoomIcon from '@material-ui/icons/Room';
import SidebarMenu from "./SidebarMenu";

function Sidebar() {
    return (
        <div className="sidebar">
            <div className="sidebarWrapper">
                <SidebarMenu title={"Rocket"} items={[["/rocket/list", <ListIcon/>, "List"], ["/rocket/analytics", <TimelineIcon/>, "Analytics"]]} />
                <SidebarMenu title={"Launchpad"} items={[["/launchpad/position", <RoomIcon/>, "Position"]]} />
                <SidebarMenu title={"Landpad"} items={[["/landpad/position", <RoomIcon/>, "Position"]]} />
                <SidebarMenu title={"Crew"} items={[["/crew/list", <ListIcon/>, "List"]]} />
            </div>
        </div>
    );
}

export default Sidebar;