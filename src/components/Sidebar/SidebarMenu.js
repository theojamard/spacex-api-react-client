import { Link } from "react-router-dom";

function SidebarMenu(props) {
    return (
        <div className="sidebarMenu">
            <h3 className="sidebarTitle">{props.title}</h3>
            <ul className="sidebarList">
                {props.items.map((item, key) => (
                    <Link key={key} to={item[0]} className="link">
                        <li className="sidebarListItem">
                            {item[1]}
                            <span className="sidebarListItemTitle">{item[2]}</span>
                        </li>
                    </Link>
                ))}
            </ul>
        </div>
    );
}

export default SidebarMenu;