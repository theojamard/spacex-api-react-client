import { useState, useEffect } from "react";
import axios from "axios";
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";

function Map() {

    const [launchpads, setLaunchpads] = useState([]);
    const [activeMarker, setActiveMarker] = useState(null);

    useEffect( () => {
        const loadLaunchpads = async () => {
            const response = await axios.get("https://api.spacexdata.com/v4/launchpads");
            setLaunchpads(response.data);
        }
        loadLaunchpads();
    }, [])

    const handleActiveMarker = (marker) => {
        if (marker === activeMarker) {
            return;
        }
        setActiveMarker(marker);
    };

    console.log(launchpads);

    return (
        <GoogleMap
            zoom={3}
            center={{lat: 34, lng: -120}}
            mapContainerClassName={"map"}
            onClick={() => setActiveMarker(null)}
        >
            {
                launchpads.map(({ id, full_name, latitude, longitude, status, launch_attempts, launch_successes, images }) => (
                    <Marker
                        key={id}
                        position={{lat: latitude, lng: longitude}}
                        onClick={() => handleActiveMarker(id)}
                    >
                        {activeMarker === id ? (
                            <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                                <div className="markerPopup">
                                    <h3 className="markerPopupTitle">{full_name}</h3>
                                    <ul className="markerPopupList">
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Status :</span> {status}</li>
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Launch Attemps :</span> {launch_attempts}</li>
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Launch Successes :</span> {launch_successes}</li>
                                    </ul>
                                    <div className="markerPopupImageContainer">
                                        <img className="markerPopupImage" src={images.large[0]} alt={full_name + 'image'}/>
                                    </div>
                                </div>
                            </InfoWindow>
                        ) : null}
                    </Marker>
            ))}
        </GoogleMap>
    );
}

export default Map;
