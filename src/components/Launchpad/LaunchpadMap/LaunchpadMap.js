import "./map.css";
import { useLoadScript} from "@react-google-maps/api";
import Map from "./Map";

function LaunchpadMap() {

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: "AIzaSyC8KrmzbKSNba6HIY3Wr2fDXEhEyXUcc0g"
    });

    return isLoaded ? <Map/> : <div>Loading...</div>
}

export default LaunchpadMap;
