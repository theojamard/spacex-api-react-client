import { useState, useEffect } from "react";
import axios from "axios";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend, LineElement, PointElement,
} from 'chart.js';
import { Line } from "react-chartjs-2";

function NumberOfRocketsLaunchedPerYear() {

    ChartJS.register(
        CategoryScale,
        LinearScale,
        PointElement,
        LineElement,
        BarElement,
        Title,
        Tooltip,
        Legend
    );

    const countOccurrencesOfCoupleArrayData = (arr, val) => arr.reduce((a, v) => (
        (v[0] === val[0] && v[1] === val[1]) ? a + 1 : a), 0);

    function getNumberOfLaunchesPerYear(rocket)
    {
        let numberOfLaunchesPerYear = [];

        for (let nbYear = 0; nbYear < years.length; nbYear++)
        {
            numberOfLaunchesPerYear.push(countOccurrencesOfCoupleArrayData(allLaunchesPerYear, [rocket, years[nbYear]]));
        }

        return numberOfLaunchesPerYear;
    }

    const [loading, setLoading] = useState(false);
    const [launches, setLaunches] = useState([]);
    const [rockets, setRockets] = useState([]);


    useEffect( () => {
        const loadLaunches = async () => {
            setLoading(true);
            const response = await axios.get("https://api.spacexdata.com/v5/launches");
            setLaunches(response.data);
            setLoading(false);

        }
        const loadRockets = async () => {
            setLoading(true);
            const response = await axios.get("https://api.spacexdata.com/v4/rockets");
            setRockets(response.data);
            setLoading(false);

        }
        loadLaunches();
        loadRockets();
    }, [])

    let years = [];
    let myRocketsArray = [];
    let allLaunchesPerYear = [];

    for (let nbLaunches = 0; nbLaunches < launches.length; nbLaunches++)
    {
        let dateOfLaunch = launches[nbLaunches].date_utc;
        let yearOfLaunch = dateOfLaunch.split('-')[0];

        if (!years.includes(yearOfLaunch))
        {
            years.push(yearOfLaunch);
        }

        allLaunchesPerYear.push([launches[nbLaunches].rocket, yearOfLaunch]);
    }

    for (let nbRockets = 0; nbRockets < rockets.length; nbRockets++)
    {
        myRocketsArray.push([rockets[nbRockets].id, rockets[nbRockets].name + ' (type: ' + rockets[nbRockets].engines.type + ')']);
    }

    const options = {
        responsive: true,
        scales: {
            x: {
                title: {
                    display: true,
                    text: 'Years'
                }
            },
            y: {
                title: {
                    display: true,
                    text: 'Number of Launch'
                }
            }
        }
    };

    return (
        (loading) ? <h4>loading API...</h4> :
        <div className="chartContainer">
            <h3 className="chartTitle">Number of Rocket Launch Per Year </h3>
            {
                (myRocketsArray[0] === undefined) ? <h4>loading data...</h4> : <Line type={"line"} data={{
                    labels: years,
                    datasets: [
                        {
                            label: myRocketsArray[0][1],
                            data: getNumberOfLaunchesPerYear(myRocketsArray[0][0]),
                            borderColor: 'rgb(255, 99, 132)',
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                        },
                        {
                            label: myRocketsArray[1][1],
                            data: getNumberOfLaunchesPerYear(myRocketsArray[1][0]),
                            borderColor: 'rgb(53, 162, 235)',
                            backgroundColor: 'rgba(53, 162, 235, 0.5)',
                        },
                        {
                            label: myRocketsArray[2][1],
                            data: getNumberOfLaunchesPerYear(myRocketsArray[2][0]),
                            borderColor: 'rgb(54,236,34)',
                            backgroundColor: 'rgba(54,236,34,0.61)',
                        },
                        {
                            label: myRocketsArray[3][1],
                            data: getNumberOfLaunchesPerYear(myRocketsArray[3][0]),
                            borderColor: 'rgb(236,96,34)',
                            backgroundColor: 'rgba(236,96,34,0.69)',
                        },
                    ],
                }} options={options} />
            }
        </div>
    );
}

export default NumberOfRocketsLaunchedPerYear;
