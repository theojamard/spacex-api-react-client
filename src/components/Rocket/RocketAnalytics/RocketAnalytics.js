import "./rocketAnalytics.css";
import NumberOfRocketsLaunchedPerYear from "./NumberOfRocketsLaunchedPerYear";

function RocketAnalytics() {
    return (
        <div className="rocketAnalytics">
            <NumberOfRocketsLaunchedPerYear/>
        </div>
    );
}

export default RocketAnalytics;
