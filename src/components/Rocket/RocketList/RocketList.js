import "./rocketList.css"
import { useState, useEffect } from "react";
import axios from "axios";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {getLaunchpads, getLandpads, getNumberOfLaunches} from "./someFunctions";

function RocketList() {

    const [rockets, setRockets] = useState([]);
    const [launchpads, setLaunchpads] = useState([]);
    const [launches, setLaunches] = useState([]);
    const [landpads, setLandpads] = useState([]);

    useEffect( () => {
        const loadRockets = async () => {
            const response = await axios.get("https://api.spacexdata.com/v4/rockets");
            setRockets(response.data);
        }
        const loadLaunchpads = async () => {
            const response = await axios.get("https://api.spacexdata.com/v4/launchpads");
            setLaunchpads(response.data);
        }
        const loadLaunches = async () => {
            const response = await axios.get("https://api.spacexdata.com/v5/launches");
            setLaunches(response.data);
        }
        const loadLandpads = async () => {
            const response = await axios.get("https://api.spacexdata.com/v4/landpads");
            setLandpads(response.data);
        }
        loadRockets();
        loadLaunchpads();
        loadLaunches();
        loadLandpads();
    }, [])

    function createCustomRocketData(name, type, active, cost_per_launch, success_rate, first_flight, nbLaunches, launchpads, landpads) {
        return { name, type, active, cost_per_launch, success_rate, first_flight, nbLaunches, launchpads, landpads };
    }

    const rocketsRows = [];

    for (let nbRocket = 0; nbRocket < rockets.length; nbRocket++)
    {
        rocketsRows.push(
            createCustomRocketData(
            rockets[nbRocket].name,
            rockets[nbRocket].engines.type,
            rockets[nbRocket].active,
            rockets[nbRocket].cost_per_launch,
            rockets[nbRocket].success_rate_pct,
            rockets[nbRocket].first_flight,
            getNumberOfLaunches(launches, rockets[nbRocket].id),
            getLaunchpads(launchpads, rockets[nbRocket].id),
            getLandpads(landpads, launches, rockets[nbRocket].id)
        ));
    }

    return (
        <div className="rocketTableContainer">
            <h1 className="rocketTableTitle">List of Rockets</h1>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead style={{backgroundColor: "#598bff"}}>
                        <TableRow>
                            <TableCell style={{color: "white"}} align="center">Name</TableCell>
                            <TableCell style={{color: "white"}} align="center">Type</TableCell>
                            <TableCell style={{color: "white"}} align="center">Active</TableCell>
                            <TableCell style={{color: "white"}} align="center">Cost Per Launch</TableCell>
                            <TableCell style={{color: "white"}} align="center">Success Rate</TableCell>
                            <TableCell style={{color: "white"}} align="center">First Flight Year</TableCell>
                            <TableCell style={{color: "white"}} align="center">Number Of Launches</TableCell>
                            <TableCell style={{color: "white"}} align="center">Launchpads</TableCell>
                            <TableCell style={{color: "white"}} align="center">Landpads</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rocketsRows.map((rocketRow) => (
                            <TableRow key={rocketRow.name}>
                                <TableCell component="th" style={{fontWeight: "bold"}} scope="row" align="center">
                                    {rocketRow.name}
                                </TableCell>
                                <TableCell align="center">{rocketRow.type}</TableCell>
                                <TableCell align="center">
                                    {
                                        rocketRow.active ? "yes" : "no"
                                    }
                                </TableCell>
                                <TableCell align="center">{rocketRow.cost_per_launch}$</TableCell>
                                <TableCell align="center">{rocketRow.success_rate}%</TableCell>
                                <TableCell align="center">
                                    {
                                        rocketRow.first_flight.split("-")[0]
                                    }
                                </TableCell>
                                <TableCell align="center">{rocketRow.nbLaunches}</TableCell>
                                <TableCell align="center">{rocketRow.launchpads}</TableCell>
                                <TableCell align="center">{rocketRow.landpads}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default RocketList;
