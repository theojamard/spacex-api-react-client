export function getLaunchpads(launchpads, rocketId)
{
    let allLaunchpads = [];

    for (let nbLaunchpads = 0; nbLaunchpads < launchpads.length; nbLaunchpads++)
    {
        if (launchpads[nbLaunchpads].rockets.includes(rocketId))
        {
            allLaunchpads.push(launchpads[nbLaunchpads].name)
        }
    }

    if (allLaunchpads.length > 1)
    {
        return allLaunchpads.join(', ')
    }
    else if (allLaunchpads.length === 1) {
        return allLaunchpads[0]
    }
    else {
        return '-'
    }
}

export function getNumberOfLaunches(launches, rocketId)
{
    let nbOfLaunches = 0;

    for (let nbLaunches = 0; nbLaunches < launches.length; nbLaunches++)
    {
        if (launches[nbLaunches].rocket.localeCompare(rocketId) === 0)
        {
            nbOfLaunches += 1;
        }
    }

    return nbOfLaunches;
}

export function getLandpads(landpads, launches, rocketId)
{
    let mixedArray = [];
    let allLandpadName = [];

    for (let nbLaunches = 0; nbLaunches < launches.length; nbLaunches++)
    {
        for (let nbLandpads = 0; nbLandpads < landpads.length; nbLandpads++)
        {
            for (let nbCores = 0; nbCores < launches[nbLaunches].cores.length; nbCores++)
            {
                if (launches[nbLaunches].cores[nbCores].landpad !== null && launches[nbLaunches].cores[nbCores].landpad.localeCompare(landpads[nbLandpads].id) === 0)
                {
                    mixedArray.push([landpads[nbLandpads].name, launches[nbLaunches].rocket])
                }
            }
        }
    }

    for (let i = 0; i < mixedArray.length; i++)
    {
        if (mixedArray[i][1].localeCompare(rocketId) === 0)
        {
            allLandpadName.push(mixedArray[i][0])
        }
    }

    let uniqLandpad = [...new Set(allLandpadName)];


    if (uniqLandpad.length === 0)
    {
        return "-"
    }
    else {
        return uniqLandpad.join(', ');
    }
}


