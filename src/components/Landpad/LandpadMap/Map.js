import { useState, useEffect } from "react";
import axios from "axios";
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";

function Map() {

    const [landpads, setLandpads] = useState([]);
    const [activeMarker, setActiveMarker] = useState(null);

    useEffect( () => {
        const loadLandpads = async () => {
            const response = await axios.get("https://api.spacexdata.com/v4/landpads");
            setLandpads(response.data);
        }
        loadLandpads();
    }, [])

    const handleActiveMarker = (marker) => {
        if (marker === activeMarker) {
            return;
        }
        setActiveMarker(marker);
    };

    console.log(landpads)

    return (
        <GoogleMap
            zoom={5}
            center={{lat: 35, lng: -100}}
            mapContainerClassName={"map"}
            onClick={() => setActiveMarker(null)}
        >
            {
                landpads.map(({ id, full_name, latitude, longitude, status, landing_attempts, landing_successes, type, images }) => (
                    <Marker
                        key={id}
                        position={{lat: latitude, lng: longitude}}
                        onClick={() => handleActiveMarker(id)}
                    >
                        {activeMarker === id ? (
                            <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                                <div className="markerPopup">
                                    <h3 className="markerPopupTitle">{full_name}</h3>
                                    <ul className="markerPopupList">
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Status :</span> {status}</li>
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Landing Attemps :</span> {landing_attempts}</li>
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Landing Successes :</span> {landing_successes}</li>
                                        <li className="markerPopupListItem"><span style={{fontWeight: "bold", color: "inherit"}}>Type :</span> {type}</li>
                                    </ul>
                                    <div className="markerPopupImageContainer">
                                        <img className="markerPopupImage" src={images.large[0]} alt={full_name + "image"}/>
                                    </div>
                                </div>
                            </InfoWindow>
                        ) : null}
                    </Marker>
                ))}
        </GoogleMap>
    );
}

export default Map;
