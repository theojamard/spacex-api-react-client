import "../../Launchpad/LaunchpadMap/map.css";
import {useLoadScript} from "@react-google-maps/api";
import Map from "./Map";

function LandpadMap() {

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: "AIzaSyC8KrmzbKSNba6HIY3Wr2fDXEhEyXUcc0g"
    });

    return isLoaded ? <Map/> : <div>Loading...</div>
}

export default LandpadMap;
