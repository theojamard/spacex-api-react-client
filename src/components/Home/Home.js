import "./home.css";

function Home() {
    return (
        <div className="home">
            <h1 className="homePadding">Dashboard Home</h1>
            <h2 className="homePadding">This dashboard is made with the <a href="https://github.com/r-spacex/SpaceX-API">SpaceX REST API</a></h2>
            <div className="homeImageContainer">
                <img alt="rocket launch" className="homeImage" src="https://camo.githubusercontent.com/2a2dfb8b139de852f33a0a268fad5a1bf5ed32b459f3193fe296a26eb9a54e4d/68747470733a2f2f6c6976652e737461746963666c69636b722e636f6d2f36353533352f34393138353134393132325f333766356335326534335f6b2e6a7067"/>
            </div>
        </div>
    );
}

export default Home;
