import "./App.css";
import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import Home from "./components/Home/Home";
import RocketAnalytics from "./components/Rocket/RocketAnalytics/RocketAnalytics"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import LaunchpadMap from "./components/Launchpad/LaunchpadMap/LaunchpadMap";
import LandpadMap from "./components/Landpad/LandpadMap/LandpadMap";
import RocketList from "./components/Rocket/RocketList/RocketList";
import CrewList from "./components/Crew/CrewList/CrewList";
import CrewProfile from "./components/Crew/CrewProfile/CrewProfile";

function App() {
  return (
      <Router>
        <Header/>
        <div className="mainContainer">
            <Sidebar/>
            <div className="main">
                <Routes>
                    <Route path="/spacex-api-react-client" element={<Home />} />
                    <Route path="/rocket/list" element={<RocketList/>} />
                    <Route path="/rocket/analytics" element={<RocketAnalytics/>} />
                    <Route path="/launchpad/position" element={<LaunchpadMap/>} />
                    <Route path="/landpad/position" element={<LandpadMap/>} />
                    <Route path="/crew/list" element={<CrewList/>} />
                    <Route path="/crew/:crewId" element={<CrewProfile/>} />
                </Routes>
            </div>
        </div>
      </Router>
  );
}

export default App;
